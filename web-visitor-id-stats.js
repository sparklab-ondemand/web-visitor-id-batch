/**
 * Created by BettlerP on 3/6/15.
 */
var fs      = require('fs');
var csv     = require('fast-csv');
var cross   = require('./lib/crossfilter');
var program = require('commander');



var inputFile;
var outputFile;
var company;
var domain;
var processType;


program
    .version('1.0.0')
    .arguments('<IP|domain> <inputfile> <company> <domain>')
    .action(function (proc, inp, comp,dom){
        if(!inp || !comp || !dom || !proc) {
            console.log('Expecting 4 input parameters.');
            program.outputHelp();
            process.exit();
        }
        if(proc!='IP' && proc!='domain') {
            console.log('  Error: Expecting IP or domain, but got "'+proc+'"');
            program.outputHelp()
            process.exit();
        }
        processType = proc;
        inputFile = inp;
        company   = comp;
        domain    = dom;
    })
    program.parse(process.argv);
if(!inputFile){
    console.log("No argument provided.")
    program.outputHelp();
    process.exit();
}
outputFile = inputFile.replace(/\.csv/,'.stat.csv');
console.log('Input file   : '+ inputFile);
console.log('Output file  : '+ outputFile);
console.log('Company      : '+ company);
console.log('Domain       : '+ domain);

var batch = {
    inputFile: inputFile,
    outputFile: outputFile,
    company: company,
    property: domain
};


var stream = fs.createReadStream(__dirname+'/'+batch.inputFile);
var crossData=[];
var crossFilter;

var sicText = fs.readFileSync(__dirname+'/data/sicmap.json', 'utf8');
var sicMap = JSON.parse(sicText);
var lineNo=0;
csv
    .fromStream(stream, {headers: true, rowDelimiter:'\n'})
    .on("data", function(data){
        crossData.push(data);
        console.log("Read line "+lineNo++);
    })
    .on("end", function(){
        console.log("done reading input file");
        var stringOut="";
        stringOut+='"'+batch.company+'"'+",";
        var today = new Date();
        var dateString = today.toISOString().substr(0,10);
        stringOut+='"'+dateString+"\","+batch.property+"\n";

        var total = crossData.length;

        // from here on, only keep unique IP addresses
        var uniqueData = [];
        var uniqueIP={};
        crossData.forEach(function(d){
            if(uniqueIP.hasOwnProperty(d._IPAddress)){
                return;
            }
            uniqueIP[d._IPAddress] = 1;
            uniqueData.push(d);
        });


        // check if the expected fields are all in the incoming data
        var validFields = ['_IPAddress','_Type','_Employees','_Region1','_ForbesLargest','_Fortune 1000','_Country code','_Revenue','_Primary Industry Code'];
        var notFoundFields = validFields.filter(function(d){ return uniqueData[0].hasOwnProperty(d)==false;});
        if(notFoundFields.length>0){
            throw new Error('Missing data fields: '+notFoundFields.join(","));
        }

        if(processType=='domain'){
            // we need to force the type field since the domain API doesn't report it obviously, they all should be BIZ
        }

        crossFilter = cross.crossfilter(uniqueData);


        var totalUnique = crossFilter.size();

        // create all dimensions
        ip        = crossFilter.dimension(function(d){return d._IPAddress;});
        isbiz     = crossFilter.dimension(function(d){return d._Type;});
        region    = crossFilter.dimension(function(d){return d._Region1;});
        employees = crossFilter.dimension(function(d,i){
            return (d.hasOwnProperty('_Employees')&&!isNaN(parseInt(d._Employees)))?parseInt(d._Employees):0;})
        forbes    = crossFilter.dimension(function(d){
            return d._ForbesLargest.toLowerCase()=='true';
        })
        fortune   = crossFilter.dimension(function(d){
            return d['_Fortune 1000'].toLowerCase()=='true';
        })
        country   = crossFilter.dimension(function(d){return d['_Country code'];})
        revenue   = crossFilter.dimension(function(d){
            return (d.hasOwnProperty('_Revenue')&&!isNaN(parseInt(d._Revenue)))?parseInt(d._Revenue):0;})
        sic       = crossFilter.dimension(function(d){return d['_Primary Industry Code'];})

        var biznobizStats = isbiz.group().all();
        stringOut+="Total,Unique, ISP, BIZ, NOMATCH\n";
        var counts = [  biznobizStats.filter(function(d){return d.key=='isp'}),
                        biznobizStats.filter(function(d){return d.key=='biz'}),
                        biznobizStats.filter(function(d){return d.key==''})];
        stringOut+=total+","+totalUnique+","
                        +(counts[0].length?counts[0][0].value:0)+","
                        +(counts[1].length?counts[1][0].value:0)+","
                        +(counts[2].length?counts[2][0].value:0)+"\n";

        function reduceAddRegion   (p, v) {  ++p.count;  p[v._Type==''?'none':v._Type]++; return p; }
        function reduceRemoveRegion(p, v) {  --p.count;  p[v._Type==''?'none':v._Type]--; return p; }
        function reduceInitialRegion()    {  return { count: 0 , isp:0, biz:0, none:0};           }

        var nRegion = region.group().reduce(reduceAddRegion,reduceRemoveRegion,reduceInitialRegion).all();
        stringOut+="EU,NA,AS,AF,SA,OC\n";
        var reg = ['Europe','North America','Asia','Africa','South America','Oceania'];
        var regStats = function(typ) {
            return reg.map(function(regi){
                var r = nRegion.filter(function(d){return d.key==regi});
                return r.length>0?r[0].value[typ]:0;
            });
        }
        stringOut+=  regStats('isp')+"\n";
        stringOut+=  regStats('biz')+"\n";

        // from here on keep biz records only
        isbiz.filter('biz')

        var nCountry= country.group().top(10);
        stringOut+= "Top countries,10\n";
        stringOut+= nCountry.map(function(d){return d.key}).reverse().join(",")+"\n";
        stringOut+= nCountry.map(function(d){return d.value}).reverse().join(",")+"\n";
        var sizeStat= employees.group(function(d){
            if(d>0){
                if(d<=     10){ return     10;}
                if(d<=     50){ return     50;}
                if(d<=    100){ return    100;}
                if(d<=    500){ return    500;}
                if(d<=   1000){ return   1000;}
                if(d<=  10000){ return  10000;}
                if(d<=  50000){ return  50000;}
                if(d<= 100000){ return 100000;}
                if(d<= 150000){ return 150000;}
                return 150001;
            }else{
                return 0;
            }
        }).all();

        var sizes = [150001,150000,100000,50000,10000,1000,500,100,50,10,0];
        stringOut+= 'By Size,'+sizes.join(",")+"\n";
        stringOut+= ','+sizes.map(function(s){
                                // search the stat with the given size
                                var r = sizeStat.filter(function(d){return d.key==s});
                                return (r.length>0)?r[0].value:0;
                            })
                         .join(",")+"\n";



        function reduceAdd   (p, v) { ++p.count;
            if(v._Revenue){ p.rev+=parseInt(v._Revenue); } return p;
        }
        function reduceRemove(p, v) { --p.count; if(v._Revenue){ p.rev-=parseInt(v._Revenue); } return p;  }
        function reduceInitial()    { return { count: 0 , rev:0}; }
        function orderValue(p)      { return p.rev;  }

        function reduceAddCoVisit   (p, v)  {
            ++p.count;if(p.companies.hasOwnProperty(v._Name)){ p.companies[v._Name]++;}else{p.companies[v._Name]=1; }return p;
        }
        function reduceRemoveCoVisit(p, v)  { --p.count;if(p.companies.hasOwnProperty(v._Name)){ p.companies[v._Name]--;}else{p.companies[v._Name]=0; }return p; }
        function reduceInitialCoVisit()     { return { count: 0 , companies:{}}; }
        function orderValueCoVisit(p)       {
            var count = 0; for (var co in p.companies){ count+= p.companies[co];  } return count;
        }

        var nSic = sic.group().reduce(reduceAdd,reduceRemove,reduceInitial);
        var t1 = nSic.all();
        var nSicStats = nSic.order(orderValue).top(10);

        for(var i=nSicStats.length;i<10;i++){
            nSicStats.push({key:"",value:{count:0,rev:0}});
        }
        stringOut+= 'Top Industries by revenue'+"\n";
        var sicC;
        nSicStats.forEach(function(d,i){
            sicC = d.key;
            // remove heading 0s
            sicC = ""+parseInt(sicC);
            sicC = sicMap.hasOwnProperty(sicC)?sicMap[sicC]:sicC;
            stringOut+= (i+1)+','+d.key+",\""+sicC+"\","+d.value.count+",$"+(d.value.rev/1000000).toFixed()+"\n";
        });


        fortune.filter(true);
        var nFort = sic.group().reduce(reduceAddCoVisit,reduceRemoveCoVisit,reduceInitialCoVisit);
        var nFortStats = nFort.order(orderValueCoVisit).top(10);
        for(var i=nFortStats.length;i<10;i++){
            nFortStats.push({key:"",value:{count:0,companies:{}}});
        }

        stringOut+= 'Top Industries Fortune by Visits'+"\n";
        nFortStats.forEach(function(d,i){
                sicC = d.key;
            if(sicC.length>0) {
                sicC = "" + parseInt(sicC);
                sicC = sicMap.hasOwnProperty(sicC)?sicMap[sicC]:sicC;
            }
            stringOut+= (i+1)+','+d.key+",\""+sicC+"\","+Object.keys(d.value.companies).length+","+d.value.count+"\n";
        })

        fortune.filterAll();


        forbes.filter(true);
        var nForb = sic.group().reduce(reduceAddCoVisit,reduceRemoveCoVisit,reduceInitialCoVisit);
        var nForbStats = nForb.order(orderValueCoVisit).top(10)
        for(var i=nForbStats.length;i<10;i++){
            nForbStats.push({key:"",value:{count:0,companies:{}}});
        }

        stringOut+= 'Top Industries Forbes by Visits'+"\n";
        nForbStats.forEach(function(d,i){
            sicC = d.key;
            if(sicC.length>0){
                sicC = ""+parseInt(sicC);
                sicC = sicMap.hasOwnProperty(sicC)?sicMap[sicC]:sicC;
            }
            stringOut+= (i+1)+','+d.key+",\""+sicC+"\","+Object.keys(d.value.companies).length+","+d.value.count+"\n";
        });

        // write header to output file
        fs.writeFileSync(batch.outputFile,stringOut);


    });


