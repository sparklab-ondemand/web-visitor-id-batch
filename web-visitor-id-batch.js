/**
 * Created by BettlerP on 3/6/15.
 */
var http    = require('http');
var fs      = require('fs');
var csv     = require('fast-csv');
var edge    = require('./lib/edge-api');
var queue   = require('./lib/rate-limit');
var XLSX    = require('xlsx');
var program = require('commander');
var prompt  = require('prompt-sync');



// max number of elements to process, or false if no limit
var toProcess=false;//10000;

var DomMatch={};
var recordMatch={};
var countAll=1;
var inProgress=0;
var ratePerSec = 20;
var queueSize=10;
var processed=0;
var errors=[];
var inputFile  = null;
var outputFile = null;
var colHeader;
var headerTag;
var processType;

program
    .version('1.0.0')
    .arguments('<IP|domain> <inputfile>')
    .action(function (proc,inp){
        if(proc!='IP' && proc!='domain') {
            console.log('  Error: Expecting IP or domain, but got "'+proc+'"');
            program.outputHelp()
        }
        if(!inp) {
            console.log('  Error: No input specified');
            program.outputHelp()
        }
        inputFile = inp;
        processType = proc;
        outputFile = inputFile.replace(".xlsx",".out.csv");
    })
    .option('-c, --count <count>', 'Process at maximum [10000] records', 10000)
    .option('-n, --column <column>', 'Column where the key can be found [0] records', 0)
    .option('-e, --header <IP address>', 'Header tag [IP address] to look for in the specified column', 'IP Address')
    .parse(process.argv);

toProcess = program.count;
colHeader = program.column;
headerTag = program.header;

console.log('Processing type      : '+ processType);
console.log('Processing input file: '+ inputFile);
console.log('Into output file     : '+ outputFile);
console.log('Column # for header  : '+ colHeader);
console.log('Header to search     : '+ headerTag);
console.log('Processing at max    : '+ toProcess+' records');

console.log("Continue? [N]");
var r = prompt();
console.log(r);
if(r!='Y'){
    console.log('Aborted');
    process.exit();
}

queue.createQueue({interval:(1.0/ratePerSec)*1000.0+10});

var apiResponseMap = {
    _duns:           'duns',
    _ForbesLargest:  'isForbesLargestPrivateCompaniesListed',
    "_Fortune 1000": 'isFortune1000Listed',
    _Name:           'name',
    _Employees:      'numberOfEmployees.value',
    _Country:        'primaryAddress.addressCountry.name',
    "_Country code": 'primaryAddress.addressCountry.isoAlpha2Code',
    _Locality:       'primaryAddress.addressLocality',
    _Region:         'primaryAddress.addressRegion',
    _Latitude:       'primaryAddress.latitude',
    _Longitude:      'primaryAddress.longitude',
    '_Postal code':  'primaryAddress.postalCode',
    '_Street address 1': 'primaryAddress.streetAddress.line1',
    '_Street address 2': 'primaryAddress.streetAddress.line2',
    '_Primary Industry Code': 'primaryIndustryCode.usSicV4',
    _Phone:          'telephone.number',
    '_Phone prefix': 'telephone.isdCode',
    _Ticker:         'tickerSymbol',
    _Revenue:        'yearlyRevenue.value'
};

var apiResponseMatchMap = {
    _dunsRM:           'duns',
    _countryCode:    'countryISOAlpha2Code',
    _ipDomainName:   'ipDomainName',
    _Type:           'ipDomainType',
    _Region1:         'region'
};

// create the batch engine, acquire token
edge = edge.edgeApi();
//edge.getToken(tokenAcqHandler.bind(undefined,fileDomAmazon,pathOutDomAmazon,colIAmazon,edge.getDomainMatch));
edge.getToken(tokenAcqHandler.bind(undefined,inputFile,outputFile,colHeader,processType));
//edge.getToken(tokenAcqHandler.bind(undefined,inputFile,outputFile,colHeader,processType=='IP'?edge.getIPResourceMatch:edge.getDomainMatch));

// the API token has been acquired
// read the input file and process each element
// elements can be either domain names or IP addresses
function tokenAcqHandler(fileIn,fileOut,col, procType) {
    // read input file
    var sheetIP = readXLSXFile(fileIn);
    var headerIP;

    // extract header
    do{
        headerIP = sheetIP.data.shift();
        if(!headerIP){
            break;
        }
    }while(headerIP[col]!=headerTag);

    if(!headerIP||headerIP[col]!=headerTag){
        console.log('Cannot find header cell: '+ headerTag);
        process.exit();
    }
    // replicate IP address field with a fixed name so that it can be identified for analytics computation
    var fieldsIP = ['_IPAddress'];
    for(var p in apiResponseMatchMap){
        fieldsIP.push(p);
    }
    for(var p in apiResponseMap){
        fieldsIP.push(p);
    }
    // write header to output file, taking into account blank cells
    var firstLine="";
    for(var i=0;i<headerIP.length;i++){
        firstLine+= '"'+(headerIP.hasOwnProperty(i)?headerIP[i]:"")+'",';
    }
    fs.writeFileSync(fileOut,firstLine+'"'+fieldsIP.join('","')+"\"\n");

    // transform input data
    // only keep the root domain
    var sheetIP;
    if(toProcess){
        sheetIP = sheetIP.data.slice(0,toProcess);
    }else{
        sheetIP = sheetIP.data;
    }
    countAll = sheetIP.length;

    // process input data
    // func can be either edge.getDomainMatch or edge.getIPResourceMatch
    processRows({data:sheetIP,col:col,header:headerIP},fileOut,procType);
}

function processRows(sheet,path,procType){
    // process elements in the queue
    while(queue.getLength()<queueSize&&sheet.data.length>0){
        var el = sheet.data.shift();
        queue.add(getVisitorID.bind(undefined,procType,el,sheet.col,sheet.header,path,processRows.bind(undefined,sheet,path,procType)));
    }
    // are there more elements to be processed
    if(processed>=countAll){
        console.log("Done");
    }
}

var resJson=[];

// process one element
function getVisitorID(procType,data,col,header,path,callback){
    var currentRow = "\"";
    var func = (procType=='IP'?edge.getIPResourceMatch:edge.getDomainMatch);
    for(var i=0;i<header.length;i++){
        currentRow+= (data.hasOwnProperty(i)?data[i].replace(/"/g,'""'):"")+'","';
    }
    currentRow = currentRow.substr(0,currentRow.length-2);
    if(!data.hasOwnProperty(col)){
        // no IP address/domain given
        fs.appendFileSync(path,currentRow+"\n");
        console.log(" inQuery:"+inProgress+" Progress: "+processed+'/'+countAll+" Empty");
        processed++;
        console.log()
        if(callback){
            callback();
        }
        return;
    }
    var currentInput = data[col].trim();
    if(recordMatch.hasOwnProperty(currentInput)){
        // we have already matched the item, reuse the result
        fs.appendFileSync(path,currentRow+","+recordMatch[currentInput]+"\n");
        console.log(" inQuery:"+inProgress+" Progress: "+processed+'/'+countAll+" Duplicate");
        processed++
        if(callback){
            callback();
        }
        return;
    }
    inProgress++;
    if(!func){
        stop=1;
    }
    var req = func(currentInput,ipRes.bind(procType));
    function ipRes(resp){
        resJson.push(resp);
        processed++;
        var stringResult="";
        inProgress--;
        if(resp.errorCode){
            stringResult = resp.errorMessage;
            fs.appendFileSync(path,currentRow+","+stringResult+"\n");
            console.log(" inQuery:"+inProgress+" Progress: "+processed+'/'+countAll+" Error: "+resp.errorMessage);
            errors.push(resp.errorMessage);
            return;
        }
        if(resp.fault){
            stringResult = resp.fault.faultstring;
            fs.appendFileSync(path,currentRow+","+stringResult+"\n");
            console.log(" inQuery:"+inProgress+" Progress: "+processed+'/'+countAll+" Fault: "+resp.fault.faultstring);
            errors.push(resp.fault.faultstring);
            if(callback){
                callback();
            }
            return;
        }
        stringResult = ""+currentInput+",";
        if(!resp.result){
            var match = resp.inquiryMatch;
            var org   = resp.organization;
            var field;
            if(match){
                for(var p in apiResponseMatchMap){
                    field = getJsonElement(match,apiResponseMatchMap[p])
                    if(this=='Domain'&&p=='_Type'){
                        field = 'biz';
                    }else{
                        field = field.replace(/"/g,'""')
                    }
                    stringResult += '"'+field+'",';
                }
            }
            if(org){
                for(var p in apiResponseMap){
                    field = getJsonElement(org,apiResponseMap[p])
                    if(typeof field == 'string' || field instanceof String){
                        field = field.replace(/"/g,'""')
                    }
                    stringResult += '"'+field+'",';
                }
                stringResult = stringResult.substr(0,stringResult.length-1);
            }
            if(recordMatch.hasOwnProperty(currentInput)){
                debug=1;
            }
            console.log(" inQuery:"+inProgress+" Progress: "+processed+'/'+countAll+" Match");
            recordMatch[currentInput] = stringResult;
            fs.appendFileSync(path,currentRow+","+stringResult+"\n");

        }else{
            switch (resp.result.actionStatus){
                case '404':
                    fs.appendFileSync(path,currentRow+","+stringResult+"\n");
                    console.log(" inQuery:"+inProgress+" Progress: "+processed+'/'+countAll+" Not found ");
                    break;
                default:
                    console.log(" inQuery:"+inProgress+" Progress: "+processed+'/'+countAll+" Error: "+resp.result.actionStatus);
                    break;
            }
        }
        if(callback){
            callback();
        }
    }
}


function readXLSXFile(file){
    var sheet=[];
    var curRow=0;

    var workbook;
    try {
        workbook = XLSX.readFile(file);
    } catch(ex){
        console.log('Problem reading input file: '+ex);
        process.exit()
    }
    var col,row,res;
    var res=[];
    var rowA=[];
    var maxCol=0;

    var sheet_name_list = workbook.SheetNames;
    try {
        sheet_name_list.forEach(function (y,index) {
            if(index>0) return;
            var worksheet = workbook.Sheets[y];
            for (z in worksheet) {
                if (z[0] === '!') continue;
                res = z.match(/([A-Z]+)(\d+)/);
                if (res.length != 3) {
                    throw "Mismatch";
                }
                row = parseInt(res[2]) - 1;
                col=0;
                col = res[1].split("").reduce(function (prev,cur,i,ar){
                    return prev*26 + cur.charCodeAt(0) - 64;
                },0)-1;
                maxCol = (col>maxCol?col:maxCol);
                if (row != curRow) {
                    curRow = row;
                    sheet.push(rowA);
                    rowA = [];
                }
                rowA[col]=(worksheet[z].v);
                var e = worksheet[z].v;
                console.log(y + "!" + z + "=" + JSON.stringify(worksheet[z].v));
            }
            sheet.push(rowA);
        })
    } catch(ex){
        console.log(ex);
    }
    return {data:sheet,maxCol:maxCol};
}
return;

function getJsonElement(json, selector, defaultVal, strictArrays){
    defaultVal = typeof defaultVal !== 'undefined' ? defaultVal : ''; // default
    strictArrays = typeof strictArrays !== 'undefined'? strictArrays : false; // default

    if(selector.length==0 || typeof json === 'undefined') { // base case
        return json ? json : defaultVal;
    }

    var cSelector; // current selector
    var newSelector;
    var pIndex = selector.indexOf('.');
    if(pIndex != -1){
        cSelector = selector.substring(0, pIndex);

        // Remove any bracketed indexes such as data[0] and tack them onto the next selector string.
        var arrayIndex = '';
        var bracketIndex = cSelector.indexOf('[');
        if(bracketIndex != -1){
            var rBracketIndex = cSelector.indexOf(']');
            if(rBracketIndex == -1)
                return defaultVal;

            arrayIndex = cSelector.substring(bracketIndex+1, rBracketIndex);
            cSelector = cSelector.substring(0, bracketIndex);
        }

        if(arrayIndex)
            newSelector = arrayIndex + '.' + selector.substring(pIndex+1, selector.length); // remaining
        else
            newSelector = selector.substring(pIndex+1, selector.length);

    } else {
        cSelector = selector;
        newSelector = '';
    }

    // fix arrays if not in strict mode
    if(!strictArrays){
        // If the json is an array and the selector isn't an index, select index 0, push current selector to next iteration.
        if((json instanceof Array) && isNaN(cSelector)){
            newSelector = selector;
            cSelector = 0;
        }
        // If the json isn't an array and the selector is an index, try string literal and drop index if nonexistent.
        else if (!(json instanceof Array) && !isNaN(cSelector)){
            // Try selection by field literal (fields names can be numbers)
            if(typeof json[cSelector] === 'undefined'){
                // If invalid, discard index and move into next selector.
                return getJsonElement(json, newSelector, defaultVal, strictArrays)
            }

        }
    }

    try{
        return getJsonElement(json[cSelector], newSelector, defaultVal, strictArrays);
    } catch(err){
        return defaultVal;
    }
}


