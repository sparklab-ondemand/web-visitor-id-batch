/**
 * Created by BettlerP on 3/11/15.
 */
var https  = require('https');

exports.edgeApi = edgeApi;
function edgeApi() {

    var edgeEnvironments = [
        {env: 'Production',     org: 'dnb-prod',    aenv: 'prod',       url: "plus.dnb.com"},
        {env: 'Staging',        org: 'dnb-prod',    aenv: 'staging',    url: "dnb-prod-staging.apigee.net"},
        {env: 'Performance',    org: 'wci-nonprod', aenv: 'perfeng',    url: "wci-nonprod-perfeng.apigee.net"},
        {env: 'QA',             org: 'wci-nonprod', aenv: 'qa',         url: "wci-nonprod-qa.apigee.net"},
        {env: 'Development',    org: 'wci-nonprod', aenv: 'dev',        url: "wci-nonprod-dev.apigee.net"}
    ];


// Development
//var consumerKey = "8SglRfPzr0d57wGuiTKC2LjGzo1AAotl";
//var consumerSecret = "xWvMjVSNefAyADjI";

// production
    var consumerKey     = "g0jvj4jmsYuMCxzGpEk8ZEWtfN4lvuBi";
    var consumerSecret  = "9HtbKrRGfGCil936";
    //var consumerKey = 'FYpaFCBwKEN516L4gnz5Nl6Z6ABOMdMY'
    //var consumerSecret = 'v3SLzr2NY8N52yiG'
    var env             = 'Production';


    var edgeBaseUrl = edgeEnvironments.filter(function (d) {
        return d.env == env;
    })[0].url;


    var currentToken = "";

    function getToken(callback, url, cKey, cSecret) {
        switch (arguments.length) {
            case 0:
                throw "Please provide at least a call back function";
            case 1:
                url = edgeBaseUrl;
            case 2:
                cKey = consumerKey;
            case 3:
                cSecret = consumerSecret;
                break;
            default:
                throw "Invalid number of arguments";
        }
        var options = {
            hostname: url,
            port: 443,
            path: '/v1/token',
            method: 'POST',
            auth: cKey + ':' + cSecret,
            headers: {
                auth: 'basic',
                "Content-Type": "application/json",
                "Origin": "www.dnb.com"
            }
        };
        var req = https.request(options,
            function (res) {
                //console.log("Got response");
            }).on('error', function (e) {
                console.log("Got error: " + e.message);
                callback(e.message);
            }).on('response', function (response) {
                var data = "";
                response.on('data', function (chunk) {
                    data += chunk;
                });
                response.on('end', function () {
                    var d;
                    try {
                        d = JSON.parse(data);
                    } catch (e) {
                        d = e.message;
                    }
                    //if (d.actionStatus != 200) {
                    //    console.log("Problem getting a token: " + d.errorMessage);
                    //    throw new Error("No token");
                    //}
                    currentToken = d.access_token;
                    callback(d);
                });
            })
        req.write('{"grant_type" : "client_credentials"}');
        req.end();
    }

    function getIPMatch(IP, callback, token) {
        if (!token) {
            token = currentToken;
        }
        getEdge("/v1/duns-search/ip/" + IP + "?level=standard", token, callback);
    }
    function getEmailMatch(email, callback, token) {
        if (!token) {
            token = currentToken;
        }
        getEdge("/v1/duns-search/email/" + email + "?level=standard", token, callback);
    }
    function getDomainMatch(domain, callback, token) {
        if (!token) {
            token = currentToken;
        }
        // remove http(s):// and the trailing slash
        domain = domain.replace(/https?\:\/\/(www)?\./,'')
        if(domain[domain.length-1] == '/'){
            domain = domain.substring(0,domain.length-1);
        }

        getEdge("/v1/duns-search/domain/" + domain + "?level=standard", token, callback);
    }
    function getIPResourceMatch(ipr, callback, token) {
        if (!token) {
            token = currentToken;
        }
        getEdge("/v1/duns-search/ipresource/" + ipr + "?ipDomainType=ANY&level=standard", token, callback);
    }

    function getEdge(path, token, callback) {
        var savedPath = path;
        var options = {
            hostname: edgeBaseUrl,
            path: path,
            method: 'GET',
            headers: {
                Authorization: "Bearer " + token,
                Origin: "www.dnb.com"
            }
        };
        var savedDate = new Date();
        //process.stdout.write("//"+savedDate.toISOString().substr(14,9)+path+"\n");

        var req = https.request(options, function (res) {
        }).on('error', function (e) {
            console.log("Got error: " + e.message);
            callback(e.message);
        }).on('response', respEdge.bind({path: path, start: savedDate, callback: callback}));
        req.write('');
        req.end();

    }

    function respEdge(response) {
        var data = "";
        var path = this.path;
        var sDate = this.start;
        var callback = this.callback;
        response.on('data', function (chunk) {
            data += chunk;
        });
        response.on('end', function () {
            var d;
            try {
                d = JSON.parse(data);
            } catch (e) {
                d = e.message;
            }
            var da = new Date();
            process.stdout.write(sDate.toISOString().substr(14,9) + "-" + da.toISOString().substr(14,9) + "  (" + (da.getTime() - sDate.getTime()) + "ms) "+path);
            callback(d);
        });
    }
    return {
        getToken: getToken,
        getDomainMatch: getDomainMatch,
        getEmailMatch: getEmailMatch,
        getIPMatch: getIPMatch,
        getIPResourceMatch: getIPResourceMatch,
        getEdge: getEdge
    }
}