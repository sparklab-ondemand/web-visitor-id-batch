exports.createQueue = createQueue;
exports.add = add;
exports.getLength = getLength;

var queue = []
var gap;
var timeOfNextRequest = 0;
var waiting = false;


function getLength(){
    return queue.length;
}

function createQueue(rateLimit) {
    gap = rateLimit.interval;
}

function next() {
    //process.stdout.write("Q: ");
    if (queue.length === 0) {
        waiting = false;
        console.log("Nothing to do");
        return;
    }

    var now = new Date().getTime();
    if (now >= timeOfNextRequest) {
        //console.log("Processing 1");
        timeOfNextRequest = now + gap;
        var item = queue.shift();
        item.func();
        if(item.callback){
            item.callback();
        }
    }else{
        //console.log("Not yet the time");
    }
    setTimeout(next, timeOfNextRequest - now);
    waiting = true;
}

function add(func,callback) {
    //process.stdout.write("Q: ");
    queue.push({func:func,callback:callback});
    if (!waiting) {
        //console.log("Next ("+queue.length+")");
        next();
    }else{
        //console.log("NONext("+queue.length+")");
    }
}
