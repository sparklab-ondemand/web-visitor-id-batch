# README #

web-visitor-id-batch is used for producing an analytics report for Web visitor ID prospect.
It is composed of 2 nodejs scripts and work with the web-visitor-id-pptx-gen Python script.

Step 1) Use web-visitor-id-batch.js to process the customer file through our IP. This will generate a raw output file (<filename>.out.csv)  
Step 2) Use web-visitor-id-stats to compute analytics on the out.csv file. This will generate a stats output file (<filename>.out.stat.csv)  
Step 3) Use web-visitor-id-pptx-gen (other repos) to produce the pptx file from the stats output file (<filename>.out.stat.csv)  


### What is this repository for? ###

The repos includes:

+ The main nodejs files
+ 2 input test files, one for domains, one for IP

### How do I get set up? ###

* Dependencies: http, fs, fast-csv, xlsx, commander, prompt-sync
* Database configuration: N/A
* How to run tests: 

Overall Steps:  
All the scripts run on the terminal and need Python and NodeJS to run.

1. Download Python and NodeJS  
    Python - https://www.python.org/downloads/  
    NodeJS - https://nodejs.org/download/  


2. Install Python & NodeJS
Make sure that /usr/local/bin is in your path  
check by running this command on your terminal  
	```
	echo $PATH
	```  
3. Clone the repos
    + sparklab-ondemand / web-visitor-id-batch - Converts input file (List of Domains / IPs) to stats file
    + sparklab-ondemand / web-visitor-id-pptx-gen - Converts Stats file to a Powerpoint Presentation, can be found at https://bitbucket.org/sparklab-ondemand/web-visitor-id-pptx-gen    

4. Dependencies   
Download all dependencies using npm and link them.  
Use the following commands to do so:

    ```
    sudo npm install -g fast-csv
    ```  
    ```
    npm link fast-csv
    ```  
    Here are some of the dependencies that you might run into: Dependencies: http, fs, fast-csv, xlsx, commander, prompt-sync...  

    In case you run into a missing dependency error while trying to run the script you will need to repeat step 4 to install and link that dependency. Finally after all dependencies are installed you will be able to run the script.  

5. Runing the script  

	There are three steps to get from a list of IP/domain names to a finished power point report:
	 
	***Step 1) Use web-visitor-id-batch.js to process the customer file through our IP. This will generate a raw output file (<filename>.out.csv)***
 
	(Repo : sparklab-ondemand / web-visitor-id-batch)

    For domain names:  
	```
	node web-visitor-id-batch.js --header Domain domain data/domainTest.xlsx
	```  
    Where:  
        "domainTest.xlsx" is the name of the inputfile and "data/domainTest.xlsx" is the fully qualified            name of the file with location  
        "Domain" is the column name in the csv  
        "domain/IP" tells the script what type of input to expect

      For IP addresses:  
	```
	node web-visitor-id-batch.js --header "IP Address" IP data/IPTest.xlsx
	```  

    You can see the help menu by typing :  
	```
	node web-visitor-id-batch.js --help
	```  

    ***Step 2) Use web-visitor-id-stats to compute analytics on the out.csv file. This will generate a stats output file (<filename>.out.stat.csv)***  
(Repo: sparklab-ondemand / web-visitor-id-batch)

    Use this command in case the input is a list of domain names  
	```
	node web-visitor-id-stats.js domain data/domainTest.out.csv test test.com
	```  

    Use this command in case the input is a list of IPs  
	```
	node web-visitor-id-stats.js IP data/domainTest.out.csv test test.com
	```  

    Where, test and test.com are the names of the Company and Company web address respectively. These will be used to populate the powerpoint report.

    ***Step 3) Use web-visitor-id-pptx-gen (other repos) to produce the pptx file from the stats output file (<filename>.out.stat.csv)***  
(Repo: sparklab-ondemand / web-visitor-id-pptx-gen)

    To run this script you need to install the pptx library (required)  
	```
	sudo pip install python-pptx
	```  

    Its time to run the script now:  
Open a terminal and navigate to where main.py is and use this command to run the script  
	```
	python main.py test.out.stat.csv
	```  
Where, "test.out.stat.csv" is the name of the file with the stats.  
This will run the script and it will create a ppt report with the same name as the csv


### Who do I talk to? ###

* Project owner: Philippe Bettler 
* Stake Holder: Nelrose Viloria